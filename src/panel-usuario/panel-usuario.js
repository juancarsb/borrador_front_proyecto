import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';
import '../login-usuario/login-usuario.js'
import '../visor-usuario/visor-usuario.js'

/**
 * @customElement
 * @polymer
 */
class PanelUsuario extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }
      </style>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
      <h2>Panel usuario</h2>
      <login-usuario on-loginevent="processLoginEvent"></login-usuario>
      <visor-usuario id="visorusuario"></visor-usuario>

    `;
  }
  static get properties() {
    return {

    };
  }

  processLoginEvent(e) {
    console.log("Recibido el evento de login");
    console.log(e);
    this.$.visorusuario.userid = e.detail.id;
  }

}

window.customElements.define('panel-usuario', PanelUsuario);
