import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-pages/iron-pages.js';
import '../visor-usuario/visor-usuario.js';
import '../login-usuario/login-usuario.js';

/**
 * @customElement
 * @polymer
 */
class TestIronPages extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }
      </style>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
      <h1 class="jumbotron">Test Iron Pages</h1>
      <select placeholder="Seleccionar componente" value="{{componentName::change}}">
        <option value=" ">Seleccionar Componente</option>
        <option value="visor">Visor Usuario</option>
        <option value="login">Login Usuario</option>
      </select>
      <iron-pages selected="[[componentName]]" attr-for-selected="component-name">
        <div component-name="visor"><visor-usuario id="10"></visor-usuario></div>
        <div component-name="login"><login-usuario></login-usuario></div>
      </iron-pages>

    `;
  }
  static get properties() {
    return {
      componentName: {
        type: String
      }
    };
  }

} //End class

window.customElements.define('test-iron-pages', TestIronPages);
