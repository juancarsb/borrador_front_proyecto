import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';

/**
 * @customElement
 * @polymer
 */
class VisorCuentas extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }
      </style>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

      <h2>Cuentas</h2>

          <dom-repeat items="{{accounts}}">
            <template>
              <div>IBAN: <span>{{item.iban}}</span></div>
              <div>Saldo: <span>{{item.balance}}</span></div>
            </template>
          </dom-repeat>

      <iron-ajax

      id="getAccounts"
      url="http://localhost:3000/apitechu/v2/accounts/{{userid}}"
      handle-as="json"
      on-response="manageAJAXResponse"
      on-error="showError"
      ></iron-ajax>
    `;
  }
  static get properties() {
    return {
      userid: {
        type: Number,
        observer: "_showAccounts"
      },
      accounts: {
        type: Array
      }
    };
  }

  manageAJAXResponse(data) {
    console.log("manageAJAXResponse");
    console.log(data.detail.response);
    this.accounts = data.detail.response;

  }
  showError(error) {
    console.log("Hubo un error");
    console.log(error);
  }

  _showAccounts(newValue, oldValue) {
    console.log("Observer del userid de visor cuentas");
    this.$.getAccounts.generateRequest();
  }
}

window.customElements.define('visor-cuentas', VisorCuentas);
